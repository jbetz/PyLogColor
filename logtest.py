#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Test for colored log output via custom stream handler
"""

__author__		= "Jochen Betz"
__copyright__	= "(C) 2018, CERN"
__version__		= "1.0.0"
__data__		= "2018/03/13"
__status__		= "DEV"



"""
Use the default streamhandler without coloring, in that case you have to set the formatting as well
"""
#import logging


"""
Use the streamhandler supporting coloring
"""
import logcolorer as logging
logLevel = logging.DEBUG
logging.getLogger().setLevel(logLevel)


import yetanotherlib


# ************************* module tests ********************

def test():

	logging.debug("This is a log message with level 'debug'")
	logging.info("This is a log message with level 'info'")
	logging.warning("This is a log message with level 'warning'")
	logging.error("This is a log message with level 'error'")
	logging.critical("This is a log message with level 'criticial'")

	try:
		a = 10 / 0
	except:
		logging.exception("This is a log message with level 'exception'")

	logging.log(logging.DEBUG, "This is a log message with level 'debug'")
	logging.log(logging.INFO, "This is a log message with level 'info'")
	logging.log(logging.WARNING, "This is a log message with level 'warning'")
	logging.log(logging.ERROR, "This is a log message with level 'error'")
	logging.log(logging.CRITICAL, "This is a log message with level 'criticial'")


	anotherBlubber = yetanotherlib.Blubber("Geneva")

# ********************** module standalone code *************


if __name__ == "__main__":
	test()

#EOF

