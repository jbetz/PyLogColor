#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Test for colored log output via custom stream handler
"""

__author__		= "Jochen Betz"
__copyright__	= "(C) 2018, CERN"
__version__		= "1.0.0"
__data__		= "2018/03/13"
__status__		= "DEV"


"""
Use the streamhandler supporting coloring
"""
import logcolorer as logging

# Change log level for this module if needed
#logLevel = logging.WARNING
#logging.getLogger().setLevel(logLevel)

class Blubber(object):
	def __init__(self, name):
		logging.debug("Creating some blubber... named '%s'" % name)
		super(Blubber, self).__init__()


megaBlubber = Blubber("Gigantis")


# ********************** module standalone code *************


if __name__ == "__main__":
	pass

#EOF

