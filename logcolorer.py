#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Enable logger using colored output
"""

__author__		= "Jochen Betz"
__copyright__	= "(C) 2018, CERN"
__version__		= "1.0.0"
__data__		= "2018/03/13"
__status__		= "DEV"

from logging import *
import sys
import platform


class ColorStreamHandlerANSI(StreamHandler):

	ANSI_COLORS = { "Normal":0,
					"Black":30,
					"Red":31,
					"Green":32,
					"Yellow":33,
					"Blue":34,
					"Magenta":35,
					"Cyan":36,
					"White":37,
					"BrightBlack":90,
					"BrightRed":91,
					"BrightGreen":92,
					"BrightYellow":93,
					"BrightBlue":94,
					"BrightMagenta":95,
					"BrightCyan":96,
					"BrightWhite":97,
				}

	LOG_LEVEL_LIMIT_DEBUG		= 10
	LOG_LEVEL_LIMIT_INFO		= 20
	LOG_LEVEL_LIMIT_WARNING		= 30
	LOG_LEVEL_LIMIT_ERROR		= 40
	LOG_LEVEL_LIMIT_CRITICAL	= 50

	LOG_LEVEL_ANSI_COLOR_DEBUG		= "BrightBlue"
	LOG_LEVEL_ANSI_COLOR_INFO		= "Green"
	LOG_LEVEL_ANSI_COLOR_WARNING	= "Yellow"
	LOG_LEVEL_ANSI_COLOR_ERROR		= "Red"
	LOG_LEVEL_ANSI_COLOR_CRITICAL	= "Magenta"

	LOG_LEVEL_ANSI_COLOR_NORMAL		= "Normal"


	def __init__(self, stream=sys.stdout):
		super().__init__(stream)


	def emit(self, record):
		record = self.addANSIColorCode(record)
		return super().emit(record)


	def flush(self):
		return super().flush()


	def addANSIColorCode(self, record):
		"""
		All non-Windows platforms are supporting ANSI escapes sequences for coloring
		"""
		logLevel = record.levelno
		if(logLevel >= self.LOG_LEVEL_LIMIT_CRITICAL):
			colorCode = self.LOG_LEVEL_ANSI_COLOR_CRITICAL
		elif(logLevel >= self.LOG_LEVEL_LIMIT_ERROR):
			colorCode = self.LOG_LEVEL_ANSI_COLOR_ERROR
		elif(logLevel >= self.LOG_LEVEL_LIMIT_WARNING):
			colorCode = self.LOG_LEVEL_ANSI_COLOR_WARNING
		elif(logLevel >= self.LOG_LEVEL_LIMIT_INFO):
			colorCode = self.LOG_LEVEL_ANSI_COLOR_INFO
		elif(logLevel >= self.LOG_LEVEL_LIMIT_DEBUG):
			colorCode = self.LOG_LEVEL_ANSI_COLOR_DEBUG
		else:
			colorCode = self.LOG_LEVEL_ANSI_COLOR_NORMAL

		ansiColorCode = self.__ansiColorCode(colorCode)
		# Embrace the message in requested color but finish with normal mode
		record.msg = ansiColorCode + str(record.msg) + self.__ansiColorCode("Normal")

		return record


	def __ansiColorCode(self, color):
		colorCode = self.ANSI_COLORS["Normal"]
		try:
			colorCode = self.ANSI_COLORS[color]
		except:
			pass # Fallback to normal

		ansiColorCode = "\x1b[{0:d}m".format(colorCode)

		return ansiColorCode


# *************************** module code ********************


"""
Basic common configuration
"""
logLevel = DEBUG
#logLevel = INFO
#logLevel = WARNING
#logLevel = ERROR


if platform.system()=='Windows':
	raise NotImplementedError("Sorry, the colored stream handler is not implemented for Windows. Check out 'https://coloredlogs.readthedocs.io/en/latest/' for possible solutions")

consoleStreamHandler = ColorStreamHandlerANSI()
loggingFormat="%(asctime)s::%(levelname)s::%(filename)s:%(funcName)s:%(lineno)s:%(message)s"
consoleFormatter = Formatter(loggingFormat)
consoleStreamHandler.setFormatter(consoleFormatter)

# Add the new stream handler for colored output, by default to stdout
getLogger().addHandler(consoleStreamHandler)
getLogger().setLevel(logLevel)

# ********************** module main function ***************
def main():
	warning("Module main function: This module is not meant to be run standalone but imported only!")


# ********************** module standalone code *************


if __name__ == "__main__":
	main()

#EOF

